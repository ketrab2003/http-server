#include "log.h"
#include <stdio.h>
#include <stdarg.h>

void log_log(int level, char *data, ...){
    va_list args;
    va_start(args, data);
    if(level > 0)
        vprintf(data, args);
    if(level < 2){
        FILE *f = fopen(LOGFILE, "a+");
        vfprintf(f, data, args);
        fclose(f);
    }
    va_end(args);
}