#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <netinet/in.h>

#include "log.h"

// return file content with added header in dest
int get_webpage(char *dest, char *path){

    // return index.html if it's root
    if(strlen(path) <= 1){
        strcpy(dest, "HTTP/1.1 301 Moved Permamently\r\nLocation: index.html");
        return 2;
    }
    
    char filepath[255] = "./assets";
    strcat(filepath, path);

    FILE *f;
    if((f = fopen(filepath, "r")) == NULL){
        // error with opening the file
        if(strcmp(path, "/404.html") != 0 && get_webpage(dest, "/404.html") == -1)
            strcpy(dest, "HTTP/1.1 404 Not Found\r\n");
        return -1;
    }

    strcpy(dest, "HTTP/1.1 200 OK\r\n\n");

    char sign = fgetc(f);
    while(sign != EOF){
        strncat(dest, &sign, 1);
        sign = fgetc(f);
    }

    return 0;
}

// receive and parse request from client socket
int parse_request(char *request, int client_socket){
    
    log_log(0, "----------Received request----------\n%s\n------------------------------------\n", request);

    // parsing GET
    if(strncmp(request, "GET", 3) == 0){
        // retrieve address
        char path[255];
        sscanf(request, "GET %s HTTP", path);

        // send corresponding webpage
        char content[2048] = "";
        int code = get_webpage(content, path);
        log_log(1, "  > Retrieve code: %d\n", code);
        send(client_socket, content, sizeof(char)*strlen(content), 0);
    
    // parsing other requests
    }else{
        char *http_header = "HTTP/1.1 501 Not Implemented\r\n";
        send(client_socket, http_header, sizeof(char)*strlen(http_header), 0);
    }

    return 0;
}

int main(){
    // reset logging file
    fclose(fopen(LOGFILE, "w"));
    log_log(0, "----------Started new session----------\n");

    // create a socket and define address
    int server_socket = socket(AF_INET, SOCK_STREAM, 0);
    log_log(1, "[+]Socket created\n");

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(8080);
    server_address.sin_addr.s_addr = INADDR_ANY;

    bind(server_socket, (struct sockaddr *) &server_address, sizeof(server_address));
    log_log(1, "[+]Socket binded\n");
    
    // listen and respond to clients
    listen(server_socket, 5);
    log_log(1, "[+]Listening...\n");

    int client_socket;
    char buffer[2048];
    while(1){
        client_socket = accept(server_socket, NULL, NULL);
        log_log(1, " - New conection: %d\n", client_socket);

        recv(client_socket, buffer, sizeof(buffer), 0);
        parse_request(buffer, client_socket);

        close(client_socket);
    }

    return 0;
}